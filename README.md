# Digitalia ChatGPT API Challenge

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: BSD

Made with love by: Carlos Joel Delgado Pizarro


## Set up the project

This project uses Docker, so it's needed to have the following versions installed —at least—:

- Docker 17.05+.
- Docker Compose 1.17+

The project will create a container with the name:

- `digitalia_gpt_local_django`

**Why Docker?** As this project will need some libraries to work, and to do some fewer steps on the process of
creating a virtual environment, activating it and installing the dependencies manually —well, yes, one command, but
still you have to write it— using docker helps a lot on reducing those steps and also for distributing the project
having the same environment and saving some headaches when wanting to run the project in different OSes.

Also, I used **Django** as the framework to build this project because it has almost all the things needed
out-of-the-box to have a web project ready in a short time.


### Run the project

To run the project, first set the environment variables, there's a file `.env.sample`, just copy/move that to `.env`
file:

`$ cp .env.sample .env`

There are some basic variables already set with default values (no need to change them as it's a local development
project, but keep in mind that when deploying to production environment, many of those values should be changed),

Go to the project's root folder and run:

`$ docker-compose up django`

the first time it will build the corresponding containers, install the dependencies of the project, when Docker
finishes doing its magic, you'll see in the terminal the following lines:

```shell
Performing system checks...

System check identified no issues (0 silenced).

Django version 4.2.2, using settings 'config.settings.local'
Development server is running at http://0.0.0.0:8000/
Using the Werkzeug debugger (https://werkzeug.palletsprojects.com/)
Quit the server with CONTROL-C.

```

That means that the project is ready to run, you can go to your browser at http://localhost:8000/, and you'll see
the main page of the project!
