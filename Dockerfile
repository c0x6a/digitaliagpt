FROM python:3.11.4-bullseye
ENV PYTHONUNBUFFERED 1
RUN mkdir /digitalia_gpt
WORKDIR /digitalia_gpt
RUN mkdir staticfiles
# RUN apt-get update -y && apt-get install -y build-essential libpq-dev
COPY ./requirements /digitalia_gpt/requirements
RUN pip install -U pip
RUN pip install -r requirements/local.txt
COPY . /digitalia_gpt
RUN chmod +x /digitalia_gpt/start.sh
