import openai


def read_csv(uploaded_file):
    try:
        csv_bytes = uploaded_file.read()
        csv_text = csv_bytes.decode("utf-8")
        return csv_text
    except Exception as e:
        return str(e)


def analyze_csv_with_chatgpt(openai_token, csv_text, custom_prompt):
    openai.api_key = openai_token

    if custom_prompt:
        prompt = f"{custom_prompt}\n{csv_text}"
    else:
        prompt = (
            f"Please analyze the following CSV data, what can you tell me about the data? what kind of analysis "
            f"can be done on it?:\n\n```{csv_text}```"
        )

    response = openai.Completion.create(model="text-davinci-002", prompt=prompt, max_tokens=200)

    return response.choices[0].text.strip()
