from unittest.mock import MagicMock, patch

from openai.error import AuthenticationError
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.test import APITestCase


class ApiTests(APITestCase):
    @patch("openai.Model.list")
    def test_authenticate_valid_token(self, mock_openai_list):
        api_url = "/api/chatgpt/authenticate/"
        response = self.client.post(path=api_url, data={"token": "valid-token"})
        assert response.status_code == HTTP_200_OK

    @patch("openai.Model.list", side_effect=AuthenticationError("Invalid API key"))
    def test_authenticate_invalid_token(self, mock_openai_list):
        api_url = "/api/chatgpt/authenticate/"
        response = self.client.post(path=api_url, data={"token": "invalid-token"})
        assert response.status_code == HTTP_400_BAD_REQUEST

    @patch("openai.Completion.create")
    @patch("digitaliagpt.chatgptapi.api.UploadFileSerializer")
    def test_upload_file(self, mock_upload_file_serializer, mock_openai_create):
        mock_response = MagicMock()
        mock_response.choices[0].text.strip.return_value = "Mocked Response from CSV data"
        mock_openai_create.return_value = mock_response

        api_url = "/api/chatgpt/upload-file/"
        response = self.client.post(path=api_url, data={"file": "dummy,text,csv"})

        assert response.status_code == HTTP_200_OK

        mock_openai_create.assert_called_once()

        response_data = response.data
        assert "answer" in response_data
        assert response_data["answer"] == "Mocked Response from CSV data"
