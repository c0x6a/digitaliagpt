import openai
from rest_framework.decorators import action
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.viewsets import ViewSet

from digitaliagpt.chatgptapi.serializers import UploadFileSerializer, ValidateTokenSerializer
from digitaliagpt.utlis.utils import analyze_csv_with_chatgpt, read_csv


class ChatGPTViewSet(ViewSet):
    serializer_class = None

    @action(
        methods=["POST"],
        detail=False,
        url_name="authenticate",
        url_path="authenticate",
        serializer_class=ValidateTokenSerializer,
    )
    def authenticate(self, request, **kwargs):
        token = request.data.get("token")
        openai.api_key = token
        try:
            openai.Model.list()
            request.session["OPENAI_TOKEN"] = token
            return Response({"detail": "Valid Token."})
        except Exception:
            request.session["OPENAI_TOKEN"] = None
            return Response({"detail": "Invalid Token."}, status=HTTP_400_BAD_REQUEST)

    @action(
        methods=["POST"],
        detail=False,
        url_name="upload_file",
        url_path="upload-file",
        serializer_class=UploadFileSerializer,
    )
    def upload_file(self, request, **kwargs):
        file_serializer = UploadFileSerializer(data=request.data)

        if file_serializer.is_valid():
            uploaded_file = file_serializer.validated_data["file"]
            custom_prompt = file_serializer.validated_data.get("custom_prompt")
            try:
                csv_text = read_csv(uploaded_file)
            except Exception as ex:
                raise APIException(ex)

            try:
                token = request.session.get("OPENAI_TOKEN")
                chatgpt_response = analyze_csv_with_chatgpt(token, csv_text, custom_prompt)
                return Response({"answer": chatgpt_response})
            except Exception as ex:
                raise APIException(ex)
        else:
            raise APIException(file_serializer.errors)
