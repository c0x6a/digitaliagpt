from django.views.generic import TemplateView


class Index(TemplateView):
    template_name = "pages/index.html"


index_view = Index.as_view()
