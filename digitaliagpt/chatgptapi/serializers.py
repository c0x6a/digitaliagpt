from rest_framework import serializers


class UploadFileSerializer(serializers.Serializer):
    file = serializers.FileField()
    custom_prompt = serializers.CharField(allow_blank=True)


class ValidateTokenSerializer(serializers.Serializer):
    token = serializers.CharField()
